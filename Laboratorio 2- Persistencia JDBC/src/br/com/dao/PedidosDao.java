package br.com.dao;

import java.util.List;

import br.com.modelo.Pedidos;

public interface PedidosDao {
	Pedidos incluirPedido(Pedidos pedido) throws Exception;

	List<Pedidos> listarPedidos(int idCliente) throws Exception;
}
