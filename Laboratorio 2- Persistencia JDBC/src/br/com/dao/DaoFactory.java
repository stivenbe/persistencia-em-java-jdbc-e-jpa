package br.com.dao;

public abstract class DaoFactory {

	//Obrigatoria Implementação
	public abstract ClientesDao getClientesDao();
	
	//Obrigatoria Implementação
	public abstract PedidosDao getPedidosDao();
}
