package br.com.dao;

import br.com.modelo.Clientes;

public interface ClientesDao {
	Clientes inserirCliente(Clientes cliente) throws Exception;

	Clientes buscarCliente(int id) throws Exception;
}
