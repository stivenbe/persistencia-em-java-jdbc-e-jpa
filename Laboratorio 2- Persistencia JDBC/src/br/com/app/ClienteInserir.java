package br.com.app;

import java.util.Date;
import java.util.Scanner;

import br.com.modelo.Clientes;
import br.com.modelo.Pedidos;
import br.com.mysqldao.MySQLDaoFactory;

public class ClienteInserir {
	
	private static Scanner entrada;
	private static String nome;
	private static String email;
	private static String descricao;
	private static double valor;
	
	public static void main(String[] args) {
		MySQLDaoFactory dao = new MySQLDaoFactory();
		Clientes c = new Clientes();
		try {
			entrada = new Scanner(System.in);
			System.out.println("-----Cadastre um cliente-----");
			System.out.println();
			
			System.out.println("Entre com nome: ");
			nome = entrada.nextLine();
			c.setNome(nome);
			System.out.println();
			
			System.out.println("Entre com email: ");
			email = entrada.nextLine();
			c.setEmail(email);

			dao.getClientesDao().inserirCliente(c);
			
			System.out.println("Cliente inserido com sucesso!");
			
			//***************************************************
			System.out.println();
			System.out.println();
			
			Pedidos p = new Pedidos();
			System.out.println("-----Cadastre um Pedido-----");
			System.out.println();
			
			p.setData(new Date());
			
			System.out.println("Entre com a Descri��o: ");
			descricao = entrada.nextLine();
			p.setDescricao(descricao);
			
			System.out.println("Entre com um valor: ");
			valor = entrada.nextDouble();
			p.setValor(valor);
			p.setIdCliente(c.getId());
			
			dao.getPedidosDao().incluirPedido(p);
			
			System.out.println("Pedido inserido com sucesso!");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
