package br.com.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.com.modelo.Clientes;
import br.com.modelo.Pedidos;
import br.com.mysqldao.MySQLDaoFactory;

public class ClienteBuscar {
	private static Scanner entrada;
	
	public static void main(String[] args) {
		MySQLDaoFactory dao = new MySQLDaoFactory();
		try {
			entrada = new Scanner(System.in);
			System.out.println("Entre com Id do Cliente");
			int id = entrada.nextInt();

			Clientes c = dao.getClientesDao().buscarCliente(id);

			System.out.println("-----Clinte-----");
			System.out.println("Nome: " + c.getNome());
			System.out.println("Email: " + c.getEmail());

			System.out.println("Cliente consultado com sucesso!");
			
			List<Pedidos> listaPedidos = new ArrayList<>();
			
			listaPedidos = dao.getPedidosDao().listarPedidos(id);
			System.out.println("Lista de Pedidos");
			for (Pedidos pedidos : listaPedidos) {
				System.out.println("Descri��o: " +pedidos.getDescricao());
				System.out.println("R$: " +pedidos.getValor());
				System.out.println("Data do Pedido: " +pedidos.getData());
			}
			
			System.out.println("Pedidos listados com sucesso!");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
