package br.com.mysqldao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import br.com.dao.ClientesDao;
import br.com.modelo.Clientes;

public class MySQLClientesDao implements ClientesDao {
	Connection cn = null;
	ResultSet rs;
	PreparedStatement stmt;

	@Override
	public Clientes inserirCliente(Clientes cliente) throws Exception {
		try {
			cn = MySQLDaoFactory.criarConexao();
			String sql = "INSERT INTO CLIENTES (NOME,EMAIL) VALUES (?,?)";
			stmt = cn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, cliente.getNome());
			stmt.setString(2, cliente.getEmail());
			stmt.execute();
			rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				cliente.setId(rs.getInt(1));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
		}
		return cliente;
	}

	@Override
	public Clientes buscarCliente(int id) throws Exception {
		Clientes cliente = null;
		
		try {
			cn = MySQLDaoFactory.criarConexao();
			
			String sql = "SELECT * FROM CLIENTES WHERE IDCLIENTE=?";
			stmt = cn.prepareStatement(sql);
			
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			while (rs.next()) {
				String nome = rs.getString("NOME");
				String email = rs.getString("EMAIL");
				
				cliente = new Clientes(id, nome, email);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return cliente;
	}
}
