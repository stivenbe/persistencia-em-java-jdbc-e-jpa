package br.com.mysqldao;

import java.sql.Connection;
import java.sql.DriverManager;

import br.com.dao.ClientesDao;
import br.com.dao.DaoFactory;
import br.com.dao.PedidosDao;

public class MySQLDaoFactory extends DaoFactory {
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	public static final String URL = "jdbc:mysql://localhost/vendas";

	public static Connection criarConexao() throws Exception {
		Class.forName(DRIVER);
		return DriverManager.getConnection(URL, "root", "root");
	}

	public ClientesDao getClientesDao() {
		return new MySQLClientesDao();
	}

	public PedidosDao getPedidosDao() {
		return new MySQLPedidosDao();
	}
}
