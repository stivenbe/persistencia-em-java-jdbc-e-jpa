package br.com.fiap.dao;

import java.sql.*;

public class ConfiguradorDao {

	protected Connection cn;
	protected PreparedStatement stmt;
	protected ResultSet rs;

	private String url = "jdbc:mysql://localhost/vendas";

	protected void abrirConexao() throws SQLException {
		cn = DriverManager.getConnection(url, "root", "root");
		System.out.println("Conex�o Aberta!");
	}

	protected void fecharConexao() throws SQLException {
		cn.close();
		if (stmt != null)
			stmt.close();
	}
}
