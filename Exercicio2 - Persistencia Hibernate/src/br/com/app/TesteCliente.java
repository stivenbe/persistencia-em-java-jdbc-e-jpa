package br.com.app;

import java.util.Date;
import java.util.List;

import br.com.dao.HelperDao;
import br.com.modelo.Clientes;
import br.com.modelo.Pedidos;

public class TesteCliente {
	public static void main(String[] args) {
		incluirPedido();
		incluirClienteNoPedido();
		listarClientesPorPedido();
	}
	
	private static void incluirPedido() {
		Pedidos pedidos1 = new Pedidos();
		pedidos1.setData(new Date());
		pedidos1.setDescricao("Jogos");
		pedidos1.setValor(200.00);
		HelperDao helper = new HelperDao();
		System.out.println(helper.salvar(pedidos1));
		
		Pedidos pedidos2 = new Pedidos();
		pedidos2.setData(new Date());
		pedidos2.setDescricao("TV");
		pedidos2.setValor(1599.00);
		System.out.println(helper.salvar(pedidos2));
		
		Pedidos pedidos3 = new Pedidos();
		pedidos3.setData(new Date());
		pedidos3.setDescricao("Carro");
		pedidos3.setValor(25000.00);
		System.out.println(helper.salvar(pedidos3));
	}

	private static void incluirClienteNoPedido() {
		HelperDao helper = new HelperDao();
		Clientes c1 = new Clientes();
		c1.setNome("michel");
		c1.setEmail("michel@mail.com");
		Clientes c2 = new Clientes();
		c2.setNome("jonas");
		c2.setEmail("joas@mail.com");
		Clientes c3 = new Clientes();
		c3.setNome("stiven");
		c3.setEmail("stiven@mail.com");
		System.out.println(helper.adicionarUsuario(1, c1));
		System.out.println(helper.adicionarUsuario(2, c2));
		System.out.println(helper.adicionarUsuario(3, c3));
	}

	private static void listarClientesPorPedido() {
		HelperDao helper = new HelperDao();
		Pedidos p = new Pedidos();
		List<Clientes> clientes = helper.listarClientes(p.getId());
		for (Clientes cliente : clientes) {
			System.out.println("ID Cliente: " + cliente.getId());
			System.out.println("Nome Cliente: " + cliente.getNome());
			System.out.println("Email Cliente: " + cliente.getEmail());
			System.out.println("Valor Pedido: " + cliente.getPedido());
			System.out.println("--------------------------------");
		}
	}
}
