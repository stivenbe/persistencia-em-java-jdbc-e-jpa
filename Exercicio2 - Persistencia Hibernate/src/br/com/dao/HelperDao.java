package br.com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.configurador.HibernateUtil;
import br.com.modelo.Clientes;
import br.com.modelo.Pedidos;

public class HelperDao {
	Session session = null;
	Transaction transaction = null;
	
	public String salvar(Pedidos pedidos) {
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			session.save(pedidos);
			transaction.commit();
			return "Pedidos salvo";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	public String salvar(Clientes clientes) {
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			session.save(clientes);
			transaction.commit();
			return "Cliente salvo";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	public String adicionarUsuario(int idPedido, int idCliente) {
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			Pedidos p = (Pedidos) session.load(Pedidos.class, idPedido);
			Clientes c = (Clientes) session.load(Clientes.class, idPedido);
			p.getClientes().add(c);
			session.save(p);
			transaction.commit();
			return "Associacao realizada";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	public String adicionarUsuario(int idPedido, Clientes clientes) {
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			Pedidos p = (Pedidos) session.load(Pedidos.class, idPedido);
			clientes.setPedido(p);
			p.getClientes().add(clientes);
			session.update(p);
			transaction.commit();
			return "Inclusao realizada";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	
	public List<Clientes> listarClientes(int idPedido) {
		List<Clientes> clientes = new ArrayList<Clientes>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Pedidos p = (Pedidos) session.load(Pedidos.class, idPedido);
			clientes = p.getClientes();
		} catch (Exception e) {
		}
		return clientes;
	}
}
