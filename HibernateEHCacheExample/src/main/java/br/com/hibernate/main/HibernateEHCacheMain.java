package br.com.hibernate.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.stat.Statistics;

import br.com.hibernate.model.Cliente;
import br.com.hibernate.util.HibernateUtil;

public class HibernateEHCacheMain {

	public static void main(String[] args) {
		
		System.out.println("Temp Dir:"+System.getProperty("java.io.tmpdir"));
		
		//Initialize Sessions
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Statistics stats = sessionFactory.getStatistics();
		System.out.println("Status ativo="+stats.isStatisticsEnabled());
		stats.setStatisticsEnabled(true);
		System.out.println("Status ativo="+stats.isStatisticsEnabled());
		
		Session session = sessionFactory.openSession();
		Session otherSession = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Transaction otherTransaction = otherSession.beginTransaction();
		
		printStats(stats, 0);
		
		Cliente cli = (Cliente) session.load(Cliente.class, 1L);
		printData(cli, stats, 1);
		
		cli = (Cliente) session.load(Cliente.class, 1L);
		printData(cli, stats, 2);
		
		//clear first level cache, so that second level cache is used
		session.evict(cli);
		cli = (Cliente) session.load(Cliente.class, 1L);
		printData(cli, stats, 3);
		
		cli = (Cliente) session.load(Cliente.class, 3L);
		printData(cli, stats, 4);
		
		cli = (Cliente) otherSession.load(Cliente.class, 1L);
		printData(cli, stats, 5);
		
		//Release resources
		transaction.commit();
		otherTransaction.commit();
		sessionFactory.close();
	}

	private static void printStats(Statistics stats, int i) {
		System.out.println("***** " + i + " *****");
		System.out.println("Fetch Count="+ stats.getEntityFetchCount());
		System.out.println("Nivel 2 Count="+ stats.getSecondLevelCacheHitCount());
		System.out.println("Nivel 2 Miss Count="+ stats.getSecondLevelCacheMissCount());
		System.out.println("Nivel 2 Put Count="+ stats.getSecondLevelCachePutCount());
	}

	private static void printData(Cliente cli, Statistics stats, int count) {
		System.out.println(count+":: Nome="+cli.getNome()+", CEP="+cli.getEndereco().getCep());
		printStats(stats, count);
	}

}
