
package br.com.ehcache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class HelloEhCache{
	
	public static void main(String[] args) {
		
		//1. Cria o manager
		CacheManager cm = CacheManager.newInstance();
		
		//2. declara o cache1 no ehcache.xml
		Cache cache = cm.getCache("cache1");
		
		//3. Adiciona uns elementos
		cache.put(new Element("1","Jan"));
		cache.put(new Element("2","Feb"));
		cache.put(new Element("3","Mar"));
		
		//4. Pega os elementos do cache
		Element ele = cache.get("2");
		
		//5. Imprimir
		String output = (ele == null ? null : ele.getObjectValue().toString());
		System.out.println(output);
		
		//6. Chave
		System.out.println(cache.isKeyInCache("3"));
		System.out.println(cache.isKeyInCache("10"));
		
		//7. Termina o cache
		cm.shutdown();
	}
	
}