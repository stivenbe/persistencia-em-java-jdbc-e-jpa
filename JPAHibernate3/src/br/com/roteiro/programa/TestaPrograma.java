package br.com.roteiro.programa;

import java.util.Date;
import java.util.List;

import br.com.roteiro.helper.GenericDao;
import br.com.roteiro.model.Clientes;
import br.com.roteiro.model.Pedidos;

public class TestaPrograma {

	public static void main(String[] args) {
		
		//Clientes
		GenericDao<Clientes> daoClientes = new GenericDao<>(Clientes.class);
		Clientes c = new Clientes();
		c.setNome("stiven");
		c.setEmail("stiven@mail.com");
		
		Clientes c2 = new Clientes();
		c2.setNome("michel");
		c2.setEmail("michel@mail.com");
				
		//Pedidos
		Pedidos pedidos1 = new Pedidos();
		pedidos1.setData(new Date());
		pedidos1.setDescricao("Jogos");
		pedidos1.setValor(200.00);
		pedidos1.setClientes(c);
		
		Pedidos pedidos2 = new Pedidos();
		pedidos2.setData(new Date());
		pedidos2.setDescricao("TV");
		pedidos2.setValor(1599.00);
		pedidos2.setClientes(c);
		
		Pedidos pedidos3 = new Pedidos();
		pedidos3.setData(new Date());
		pedidos3.setDescricao("Carro");
		pedidos3.setValor(25000.00);
		pedidos3.setClientes(c2);
		
		//Associar
		c.getPedidos().add(pedidos1);
		c.getPedidos().add(pedidos2);
		c2.getPedidos().add(pedidos3);
		
		daoClientes.adicionar(c);
		daoClientes.adicionar(c2);
		
		//----------------------------Lista Pedidos
	
		GenericDao<Pedidos> daoPedidos = new GenericDao<>(Pedidos.class);
		
		List<Pedidos> pedidos = daoPedidos.listar();
		for(Pedidos pedido: pedidos) {
			System.out.println("--------------------------------");
			System.out.println("ID Pedido: " + pedido.getId());
			System.out.println("Nome Pedido: " + pedido.getDescricao());
			System.out.println("Data Pedido: " + pedido.getData());
			System.out.println("Valor Pedido: " + pedido.getValor());
			System.out.println("Cliente: " +pedido.getClientes().getNome());
		}
	}

}
