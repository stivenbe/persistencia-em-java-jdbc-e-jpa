package br.com.app;

import java.util.Set;

import br.com.helper.ForumHelper;
import br.com.modelo.Forum;
import br.com.modelo.Usuario;

public class TesteForum {
	public static void main(String[] args) {
		incluirForum();
		incluirUsuarionoForum();
		listarUsuariosPorForum();
	}

	private static void incluirForum() {
		Forum forum = new Forum();
		forum.setAssunto("Avaliação");
		forum.setDescricao("Avaliação da disciplina Persistência");
		ForumHelper helper = new ForumHelper();
		System.out.println(helper.salvar(forum));
		
		Forum forum2 = new Forum();
		forum2.setAssunto("Avaliação");
		forum2.setDescricao("Avaliação da disciplina JDBC");
		System.out.println(helper.salvar(forum2));
		
		Forum forum3 = new Forum();
		forum3.setAssunto("Avaliação");
		forum3.setDescricao("Avaliação da disciplina WEB");
		System.out.println(helper.salvar(forum3));
	}

	private static void incluirUsuarionoForum() {
		ForumHelper helper = new ForumHelper();
		Usuario u1 = new Usuario();
		u1.setNome("michel");
		u1.setEmail("michel@mail.com");
		Usuario u2 = new Usuario();
		u2.setNome("jonas");
		u2.setEmail("joas@mail.com");
		Usuario u3 = new Usuario();
		u3.setNome("stiven");
		u3.setEmail("stiven@mail.com");
		System.out.println(helper.adicionarUsuario(1, u1));
		System.out.println(helper.adicionarUsuario(2, u2));
		System.out.println(helper.adicionarUsuario(3, u3));
	}

	private static void listarUsuariosPorForum() {
		ForumHelper helper = new ForumHelper();
		Forum f = new Forum();
		Set<Usuario> usuarios = helper.listarUsuarios(f.getId());
		for (Usuario usuario : usuarios) {
			System.out.println("ID Usuario: " + usuario.getId());
			System.out.println("Nome Usuario: " + usuario.getNome());
			System.out.println("Email Usuario: " + usuario.getEmail());
			System.out.println("--------------------------------");
		}
	}

}
