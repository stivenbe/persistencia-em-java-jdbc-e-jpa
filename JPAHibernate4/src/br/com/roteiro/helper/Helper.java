package br.com.roteiro.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.roteiro.model.Agenda;
import br.com.roteiro.model.Matmed;
import br.com.roteiro.model.Paciente;
import br.com.roteiro.model.Procedimento;

public class Helper {
	private EntityManager em;

	public Helper(EntityManager em) {
		this.em = em;
	}
	
	public void salvar(Agenda agenda) throws Exception {
		try {
			em.getTransaction().begin();
			em.persist(agenda);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Agenda> listarAgendas() {
		Query query = em.createQuery("select a from Agenda a");
		return query.getResultList();
	}
	
	public Paciente buscarFuncionario(String cpfPaciente) {
		Query query = em.createQuery("select p from Paciente f where cpf = :cpf");
		query.setParameter("cpf", cpfPaciente);
		Paciente a = (Paciente) query.getSingleResult();
		return a;
	}
	
	public Paciente buscarPaciente(String cpf) {
		Paciente paciente = this.em.find(Paciente.class, cpf);
		return paciente;
	}

	public Agenda buscarAgenda(int id) {
		Agenda agenda = this.em.find(Agenda.class, id);
		return agenda;
	}
	
	@SuppressWarnings("unchecked")
	public List<Paciente> listarTodos() {
		Query query = em.createNamedQuery("Paciente.findAll");
		return query.getResultList();
	}
	
	public void salvarP(Procedimento procedimento) throws Exception {
		try {
			em.getTransaction().begin();
			em.persist(procedimento);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void salvarM(Matmed matmed) throws Exception {
		try {
			em.getTransaction().begin();
			em.persist(matmed);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
