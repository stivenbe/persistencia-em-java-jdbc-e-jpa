package br.com.roteiro.programa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.roteiro.helper.Helper;
import br.com.roteiro.model.Agenda;
import br.com.roteiro.model.Matmed;
import br.com.roteiro.model.Paciente;
import br.com.roteiro.model.Procedimento;

public class TestaAgenda {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAHibernate4");
		EntityManager em = emf.createEntityManager();
				
		Agenda agenda = new Agenda();
		agenda.setId(1);
		agenda.setDescricao("Consulta");
		agenda.setData(LocalDate.now());
		agenda.setHora(LocalDate.now());
		
		Paciente paciente = new Paciente();
		paciente.setCpf("4444");
		paciente.setNome("B");
		paciente.setTelefone(1123);
		paciente.setDataNasc(LocalDate.of(1995, 12, 28));
		paciente.getAgenda().add(agenda);
		
		Procedimento procedimento = new Procedimento();
		procedimento.setCpfpac("343");
		procedimento.setDescricao("123");
		procedimento.setPreco(23.00);
		procedimento.setPaciente(paciente);
		
		Matmed matmed = new Matmed();
		matmed.setCpfpac("23");
		matmed.setDescricao("123");
		matmed.setFabricante("Dorflex");
		matmed.setPreco(23.00);
		matmed.setPaciente(paciente);
		
		incluirAgenda(em, agenda, paciente, procedimento, matmed);
		em.close();
	    em = emf.createEntityManager();
		
	    em.close();
	    em = emf.createEntityManager();
		buscarAgenda(em, agenda);
		buscarAgenda(em, agenda);
		buscarAgenda(em, agenda);

		buscarPaciente(em, paciente);
		
		em = emf.createEntityManager();
		LocalDateTime inicio = LocalDateTime.now();
		listarAgendas(em);
		buscarAgenda(em, 1);
		
		
		LocalDateTime fim = LocalDateTime.now();
		System.out.println(ChronoUnit.MILLIS.between(inicio, fim));
		
		inicio = LocalDateTime.now();
		listarAgendas(em);
		buscarAgenda(em, 1);
		fim = LocalDateTime.now();
		System.out.println(ChronoUnit.MILLIS.between(inicio, fim));
		
	    
		em.close();
		emf.close();
	}
	
	public static void incluirAgenda(EntityManager em, Agenda agenda, Paciente paciente, Procedimento procedimento, Matmed matmed) {
		Helper dao = new Helper(em);
		
		agenda.getPacientes().add(paciente);
		paciente.getProcedimentos().add(procedimento);
		paciente.getMatmed().add(matmed);
		
		try {
			dao.salvar(agenda);
			System.out.println("Agenda salva com sucesso!");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static void incluirAgenda(EntityManager em) {
		Helper dao = new Helper(em);
		
		Agenda agenda = new Agenda();
		agenda.setId(1);
		agenda.setDescricao("Consulta");
		agenda.setData(LocalDate.of(2017, 12, 16));
		agenda.setHora(LocalDate.now());
		
		Paciente paciente = new Paciente();
		paciente.setCpf("4444");
		paciente.setNome("B");
		paciente.setTelefone(1123);
		paciente.setDataNasc(LocalDate.of(1995, 12, 28));
		paciente.getAgenda().add(agenda);
		agenda.getPacientes().add(paciente);
		
		try {
			dao.salvar(agenda);
			System.out.println("Agenda salva com sucesso!");
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static void listarAgendas(EntityManager em) {
		Helper dao = new Helper(em);
		List<Agenda> agendas = dao.listarAgendas();
		for (Agenda agenda : agendas) {
			System.out.println(agenda.getId() + ": " + agenda.getDescricao());
		}
	}

	private static void buscarAgenda(EntityManager em, int id) {
		Helper dao = new Helper(em);
		Agenda f = dao.buscarAgenda(id);
		System.out.println(f.getId() + ": " + f.getDescricao());
	}
	
	private static void buscarAgenda(EntityManager em, Agenda agenda) {
		Helper dao = new Helper(em);
		agenda = dao.buscarAgenda(agenda.getId());
		System.out.println(agenda.getId() + ": " + agenda.getDescricao());
	}
	
	private static void buscarPaciente(EntityManager em, Paciente paciente) {
		Helper dao = new Helper(em);
		paciente = dao.buscarPaciente(paciente.getCpf());
		System.out.println(paciente.getNome() + ": " + paciente.getCpf());
	}

}
