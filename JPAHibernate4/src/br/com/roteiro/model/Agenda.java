package br.com.roteiro.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AGENDA")
public class Agenda implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	private int id;

	@Column(name = "DATA")
	private LocalDate data;

	@Column(name = "HORA")
	private LocalDate hora;

	@Column(name = "DESCRICAO")
	private String descricao;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "AGENDA_PACIENTE", joinColumns = {@JoinColumn(name = "AGENDA_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "PACIENTE_CPF", nullable = false, updatable = false) })
	private Set<Paciente> pacientes = new HashSet<Paciente>();

	public Agenda() {
		super();
	}

	public Agenda(int id, LocalDate data, LocalDate hora, String descricao, Set<Paciente> pacientes) {
		super();
		this.id = id;
		this.data = data;
		this.hora = hora;
		this.descricao = descricao;
		this.pacientes = pacientes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalDate getHora() {
		return hora;
	}

	public void setHora(LocalDate hora) {
		this.hora = hora;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(Set<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

}
