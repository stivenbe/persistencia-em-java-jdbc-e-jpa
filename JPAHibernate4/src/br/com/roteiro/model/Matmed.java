package br.com.roteiro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MATMED")
public class Matmed {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "DESCRICAO")
	private String descricao;

	@Column(name = "PRECO")
	private double preco;

	@Column(name = "FABRICANTE")
	private String fabricante;

	@Column(name = "CPFPAC")
	private String cpfpac;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDPACIENTE")
	private Paciente paciente;

	public Matmed() {
		super();
	}

	public Matmed(int id, String descricao, double preco, String fabricante, String cpfpac, Paciente paciente) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.preco = preco;
		this.fabricante = fabricante;
		this.cpfpac = cpfpac;
		this.paciente = paciente;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getCpfpac() {
		return cpfpac;
	}

	public void setCpfpac(String cpfpac) {
		this.cpfpac = cpfpac;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
}
