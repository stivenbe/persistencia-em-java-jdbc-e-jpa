package br.com.roteiro.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PACIENTE")
public class Paciente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CPF", unique = true, nullable = false)
	private String cpf;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "DATANASC")
	private LocalDate dataNasc;

	@Column(name = "TELEFONE")
	private int telefone;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "pacientes")
	private Set<Agenda> agenda = new HashSet<Agenda>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "paciente")
	private List<Procedimento> procedimentos = new ArrayList<Procedimento>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "paciente")
	private List<Matmed> matmed = new ArrayList<Matmed>();

	
	public Paciente() {
		super();
	}

	public Paciente(String cpf, String nome, LocalDate dataNasc, int telefone, Set<Agenda> agenda,
			List<Procedimento> procedimentos, List<Matmed> matmed) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.dataNasc = dataNasc;
		this.telefone = telefone;
		this.agenda = agenda;
		this.procedimentos = procedimentos;
		this.matmed = matmed;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDataNasc() {
		return dataNasc;
	}

	public void setDataNasc(LocalDate dataNasc) {
		this.dataNasc = dataNasc;
	}

	public int getTelefone() {
		return telefone;
	}

	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}

	public Set<Agenda> getAgenda() {
		return agenda;
	}

	public void setAgenda(Set<Agenda> agenda) {
		this.agenda = agenda;
	}

	public List<Procedimento> getProcedimentos() {
		return procedimentos;
	}

	public void setProcedimentos(List<Procedimento> procedimentos) {
		this.procedimentos = procedimentos;
	}

	public List<Matmed> getMatmed() {
		return matmed;
	}

	public void setMatmed(List<Matmed> matmed) {
		this.matmed = matmed;
	}
	
	

}
